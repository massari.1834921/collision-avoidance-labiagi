# collision avoidance-labiagi

- aggiungere i contenuti di questa repository alla cartella src del proprio workspace

- aggiungere nella cartella src del proprio workspace anche i contenuti della repository al seguente link: [https://gitlab.com/massari.1834921/teleop_twist_keyboard-collision-avoidance.git](https://gitlab.com/massari.1834921/teleop_twist_keyboard-collision-avoidance.git)

- dal workspace aprire un terminale da cui: `catkin build` e poi `rosrun collision-avoidance-labiagi CollisionAvoidance`

- da un altro terminale `rosrun stage_ros stageros <worldfile>`

- dal workspace aprire un terzo terminale da cui: `rosrun teleop_twist_keyboard_cpp teleop_twist_keyboard`
- sarà possibile muoversi a destra(tramite il tasto `l`) e a sinistra(tramite il tasto `j`)

