#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <laser_geometry/laser_geometry.h>
#include <tf/transform_listener.h>
#include <math.h>


geometry_msgs::Twist v;
ros::Publisher pub;

void cmd_callback(const geometry_msgs::Twist::ConstPtr& msg){
  v= *msg; 
}


void scan_callback(const sensor_msgs::LaserScan::ConstPtr& scan_in){
   
    laser_geometry::LaserProjection projector_;
    tf::TransformListener listener;
    sensor_msgs::PointCloud cloud;

  if(!listener.waitForTransform(scan_in->header.frame_id,"/base_laser_link",scan_in->header.stamp + ros::Duration().fromSec(scan_in->ranges.size()*scan_in->time_increment),ros::Duration(1.0))){       
     return;
     }
    projector_.transformLaserScanToPointCloud("/base_laser_link",*scan_in,cloud,listener); 


v.linear.x=0.4;
v.linear.y=0.4;
v.angular.z=0.0;
pub.publish(v);

 for(int i = 0; i<cloud.points.size(); i++){

 float dist = sqrt(cloud.points[i].x*cloud.points[i].x + cloud.points[i].y*cloud.points[i].y);
   if(dist<0.2){ 
   //se l'ostacolo è troppo vicino va indietro e ruota leggermente 
   v.linear.x += - (1/dist);
   v.angular.z += -(1/dist)*0.3; 

  pub.publish(v);}
  }
}

int main(int argc, char* argv[]){

  ros::init(argc, argv, "CollisionAvoidance");
  ros::NodeHandle n;

  ros::Subscriber laser_sub = n.subscribe("/base_scan", 100, scan_callback);
  ros::Subscriber cmd_sub = n.subscribe("/cmd_vel", 100, cmd_callback);
  pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 100);   
  ros::spin();
  

  return 0;
}
